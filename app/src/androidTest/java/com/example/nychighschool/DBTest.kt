package com.example.nychighschool

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.example.nychighschool.data.database.HighSchoolDataBase
import com.example.nychighschool.data.database.dao.HighSchoolDao
import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.IsEqual.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DBTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var dao: HighSchoolDao
    private lateinit var highSchoolDataBase: HighSchoolDataBase

    @Before
    fun setUp() {
        highSchoolDataBase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            HighSchoolDataBase::class.java
        ).build()
        dao = highSchoolDataBase.getHighSchoolDao()
    }

    @After
    fun tearDown() {
        highSchoolDataBase.close()
    }

    @Test
    fun saveMovieTest(): Unit = runBlocking {
        val sample = HighSchoolListItem("1234", "School@gmai", "School Name", "123456789")
        val highSchool = listOf(
            sample,
            HighSchoolListItem("12345", "School2@gmai", "School Name2", "1234567892")

        )
        dao.saveHighSchool(highSchool)
        val highSchoolList = dao.getHighSchool()
        assertThat(highSchoolList[0], equalTo(sample))
    }
}