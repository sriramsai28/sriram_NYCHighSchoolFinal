package com.example.nychighschool.data.model.listdata

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Keep
@Entity
data class HighSchoolListItem(
    @PrimaryKey
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_email")
    val schoolEmail: String?,
    @SerializedName("school_name")
    val schoolName: String?,
    @SerializedName("phone_number")
    val phoneNumber: String?
)
