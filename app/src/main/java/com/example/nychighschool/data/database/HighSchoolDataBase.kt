package com.example.nychighschool.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.nychighschool.data.database.dao.HighSchoolDao
import com.example.nychighschool.data.model.listdata.HighSchoolListItem

@Database(
    entities = [HighSchoolListItem::class],
    version = 1,
    exportSchema = false
)
abstract class HighSchoolDataBase : RoomDatabase() {
    abstract fun getHighSchoolDao(): HighSchoolDao
}
