package com.example.nychighschool.data.api

import com.example.nychighschool.data.details.DetailsList
import com.example.nychighschool.data.model.listdata.HighSchoolList
import retrofit2.Response
import retrofit2.http.GET

interface NYCSchoolService {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolList(): Response<HighSchoolList>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolDetails(): Response<DetailsList>
}
