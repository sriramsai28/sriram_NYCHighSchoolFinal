package com.example.nychighschool.data.repository.datasource.details

import com.example.nychighschool.data.details.DetailsList
import retrofit2.Response

interface SchoolDetailsRemoteDataSource {
    suspend fun getHighSchoolFromAPI(): Response<DetailsList>
}
