package com.example.nychighschool.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import com.example.nychighschool.data.model.listdata.HighSchoolListItem

@Dao
interface HighSchoolDao {

    @Insert(onConflict = REPLACE)
    suspend fun saveHighSchool(schoolList: List<HighSchoolListItem>)

    @Query("DELETE From HighSchoolListItem")
    suspend fun deleteAllHighSchool()

    @Query("SELECT * from HighSchoolListItem")
    suspend fun getHighSchool(): List<HighSchoolListItem>
}
