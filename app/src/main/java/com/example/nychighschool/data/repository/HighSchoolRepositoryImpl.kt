package com.example.nychighschool.data.repository

import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import com.example.nychighschool.data.repository.datasource.list.HighSchoolCacheDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolLocalDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolRemoteDataSource
import com.example.nychighschool.domain.repository.HighSchoolRepository
import java.lang.Exception
import javax.inject.Inject

class HighSchoolRepositoryImpl @Inject constructor(
    private val highSchoolCacheDataSource: HighSchoolCacheDataSource,
    private val highSchoolLocalDataSource: HighSchoolLocalDataSource,
    private val highSchoolRemoteDataSource: HighSchoolRemoteDataSource
) : HighSchoolRepository {
    override suspend fun getHighSchoolList(): List<HighSchoolListItem> {
        return getHighSchoolFromCache()
    }

    suspend fun getHighSchoolFromAPI(): List<HighSchoolListItem> {
        var highSchoolList: List<HighSchoolListItem>? = ArrayList()
        try {
            val response = highSchoolRemoteDataSource.getHighSchoolFromAPI()
            val body = response.body()
            if (body != null) {
                // Given time I would implement pagination to get data as user scrolls.
                highSchoolList = body
            }
        } catch (exception: Exception) {
        }

        return highSchoolList!!
    }

    suspend fun getHighSchoolFromDB(): List<HighSchoolListItem> {
        var highSchoolList: List<HighSchoolListItem> = ArrayList()
        try {
            highSchoolList = highSchoolLocalDataSource.getHighSchoolFromDB()
        } catch (exception: Exception) {
            // TODO: handle exception.
        }

        if (highSchoolList.isNotEmpty()) {
            return highSchoolList
        } else {
            highSchoolList = getHighSchoolFromAPI()
            highSchoolLocalDataSource.saveHighSchoolToDB(highSchoolList)
        }
        return highSchoolList
    }

    suspend fun getHighSchoolFromCache(): List<HighSchoolListItem> {
        var highSchoolList: List<HighSchoolListItem> = ArrayList()
        try {
            highSchoolList = highSchoolCacheDataSource.getHighSchoolFromCache()
        } catch (exception: Exception) {
        }

        if (highSchoolList.isNotEmpty()) {
            return highSchoolList
        } else {
            highSchoolList = getHighSchoolFromDB()
            highSchoolCacheDataSource.saveHighSchoolToCache(highSchoolList)
        }

        return highSchoolList
    }
}
