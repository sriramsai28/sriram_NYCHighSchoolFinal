package com.example.nychighschool.data.repository.datasource.list

import com.example.nychighschool.data.model.listdata.HighSchoolListItem

interface HighSchoolLocalDataSource {
    suspend fun getHighSchoolFromDB(): List<HighSchoolListItem>

    suspend fun saveHighSchoolToDB(highSchool: List<HighSchoolListItem>)

    suspend fun clearAll()
}
