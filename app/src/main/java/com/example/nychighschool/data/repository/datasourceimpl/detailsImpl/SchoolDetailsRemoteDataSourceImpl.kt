package com.example.nychighschool.data.repository.datasourceimpl.detailsImpl

import com.example.nychighschool.data.api.NYCSchoolService
import com.example.nychighschool.data.details.DetailsList
import com.example.nychighschool.data.repository.datasource.details.SchoolDetailsRemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class SchoolDetailsRemoteDataSourceImpl @Inject constructor(
    private val nycSchoolService: NYCSchoolService
) : SchoolDetailsRemoteDataSource {
    override suspend fun getHighSchoolFromAPI(): Response<DetailsList> {
        return nycSchoolService.getSchoolDetails()
    }
}
