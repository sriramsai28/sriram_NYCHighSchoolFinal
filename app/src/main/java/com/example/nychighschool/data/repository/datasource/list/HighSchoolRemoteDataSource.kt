package com.example.nychighschool.data.repository.datasource.list

import com.example.nychighschool.data.model.listdata.HighSchoolList
import retrofit2.Response

interface HighSchoolRemoteDataSource {
    suspend fun getHighSchoolFromAPI(): Response<HighSchoolList>
}
