package com.example.nychighschool.data.repository.datasourceimpl

import com.example.nychighschool.data.details.DetailsListItem
import com.example.nychighschool.data.repository.datasource.details.SchoolDetailsRemoteDataSource
import com.example.nychighschool.domain.repository.SchoolDetailsRepository
import javax.inject.Inject

class SchoolDetailsRepositoryImpl @Inject constructor(
    private val schoolDetailsRemoteDataSource: SchoolDetailsRemoteDataSource
) : SchoolDetailsRepository {
    override suspend fun getHighSchoolList(schoolName: String): DetailsListItem? {
        val details = schoolDetailsRemoteDataSource.getHighSchoolFromAPI().body().let {
            it?.find {
                it.dbn == schoolName
            }
        }
        return details
    }
}
