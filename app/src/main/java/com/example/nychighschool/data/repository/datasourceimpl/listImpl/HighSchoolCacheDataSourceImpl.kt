package com.example.nychighschool.data.repository.datasourceimpl.listImpl

import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import com.example.nychighschool.data.repository.datasource.list.HighSchoolCacheDataSource

class HighSchoolCacheDataSourceImpl : HighSchoolCacheDataSource {
    private var highSchoolList = ArrayList<HighSchoolListItem>()
    override suspend fun getHighSchoolFromCache(): List<HighSchoolListItem> {
        return highSchoolList
    }

    override suspend fun saveHighSchoolToCache(highSchool: List<HighSchoolListItem>) {
        highSchoolList.clear()
        highSchoolList = ArrayList(highSchool)
    }
}
