package com.example.nychighschool.data.repository.datasourceimpl.listImpl

import com.example.nychighschool.data.database.dao.HighSchoolDao
import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import com.example.nychighschool.data.repository.datasource.list.HighSchoolLocalDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class HighSchoolLocalDataSourceImpl @Inject constructor(
    private val highSchoolDao: HighSchoolDao
) : HighSchoolLocalDataSource {
    override suspend fun getHighSchoolFromDB(): List<HighSchoolListItem> {
        return highSchoolDao.getHighSchool()
    }

    override suspend fun saveHighSchoolToDB(highSchool: List<HighSchoolListItem>) {
        CoroutineScope(Dispatchers.IO).launch {
            highSchoolDao.saveHighSchool(highSchool)
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            highSchoolDao.deleteAllHighSchool()
        }
    }
}
