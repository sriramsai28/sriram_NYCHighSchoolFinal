package com.example.nychighschool.data.repository.datasourceimpl.listImpl

import com.example.nychighschool.data.api.NYCSchoolService
import com.example.nychighschool.data.model.listdata.HighSchoolList
import com.example.nychighschool.data.repository.datasource.list.HighSchoolRemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class HighSchoolRemoteDataSourceImpl @Inject constructor(
    private val nycSchoolService: NYCSchoolService
) : HighSchoolRemoteDataSource {
    override suspend fun getHighSchoolFromAPI(): Response<HighSchoolList> =
        nycSchoolService.getSchoolList()
}
