package com.example.nychighschool.data.repository.datasource.list

import com.example.nychighschool.data.model.listdata.HighSchoolListItem

interface HighSchoolCacheDataSource {
    suspend fun getHighSchoolFromCache(): List<HighSchoolListItem>

    suspend fun saveHighSchoolToCache(highSchool: List<HighSchoolListItem>)
}
