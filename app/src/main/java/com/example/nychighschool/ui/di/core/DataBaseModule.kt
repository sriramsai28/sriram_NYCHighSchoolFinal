package com.example.nychighschool.ui.di.core

import android.content.Context
import androidx.room.Room
import com.example.nychighschool.data.database.HighSchoolDataBase
import com.example.nychighschool.data.database.dao.HighSchoolDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {
    @Singleton
    @Provides
    fun getHighSchoolDataBase(context: Context): HighSchoolDataBase {
        return Room.databaseBuilder(context, HighSchoolDataBase::class.java, "NYCSchool").build()
    }

    @Singleton
    @Provides
    fun getHighSchoolDAO(highSchoolDataBase: HighSchoolDataBase): HighSchoolDao {
        return highSchoolDataBase.getHighSchoolDao()
    }
}
