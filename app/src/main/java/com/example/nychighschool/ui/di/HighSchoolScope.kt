package com.example.nychighschool.ui.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class HighSchoolScope()
