package com.example.nychighschool.ui.view

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HighSchoolDetailView(
    schoolName: String,
    viewModel: HighSchoolViewModel
) {
    rememberCoroutineScope().launch {
        viewModel.getSchoolDetails(schoolName)
    }
    val scaffoldState = rememberScaffoldState()
    val details = viewModel.detailsLiveData.observeAsState()
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("New York State High School", color = Color.White) },
                backgroundColor = Color.Blue
            )
        },

        scaffoldState = scaffoldState,
        backgroundColor = Color.Cyan
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            if (details.value != null) {
                details.value.let { detail ->
                    Spacer(modifier = Modifier.padding(20.dp))
                    Text(
                        text = "School Name: ${detail?.school_name}",
                        textAlign = TextAlign.Right,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    Spacer(modifier = Modifier.padding(20.dp))

                    Text(
                        text = "School SAT Average: ${detail?.sat_critical_reading_avg_score}",
                        textAlign = TextAlign.Right,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    Spacer(modifier = Modifier.padding(20.dp))

                    Text(
                        text = "School Math Score: ${detail?.sat_math_avg_score}",
                        textAlign = TextAlign.Right,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    Spacer(modifier = Modifier.padding(20.dp))

                    Text(
                        text = "School Writing Score: ${detail?.sat_writing_avg_score}",
                        textAlign = TextAlign.Right,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    Spacer(modifier = Modifier.padding(20.dp))

                    Text(
                        text = "School Reading Score: ${detail?.sat_critical_reading_avg_score}",
                        textAlign = TextAlign.Right,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                }
            } else {
                Text(
                    text = "No data found for the School!",
                    textAlign = TextAlign.Right,
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    fontSize = 20.sp
                )
            }
        }
    }
}
