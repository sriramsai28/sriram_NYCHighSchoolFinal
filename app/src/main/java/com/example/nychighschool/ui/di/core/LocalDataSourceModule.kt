package com.example.nychighschool.ui.di.core

import com.example.nychighschool.data.database.dao.HighSchoolDao
import com.example.nychighschool.data.repository.datasource.list.HighSchoolLocalDataSource
import com.example.nychighschool.data.repository.datasourceimpl.listImpl.HighSchoolLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDataSourceModule {
    @Singleton
    @Provides
    fun getHighSchoolLocalDataModule(
        highSchoolDao: HighSchoolDao
    ): HighSchoolLocalDataSource {
        return HighSchoolLocalDataSourceImpl(highSchoolDao)
    }
}
