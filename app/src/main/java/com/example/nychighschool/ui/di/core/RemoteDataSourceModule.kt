package com.example.nychighschool.ui.di.core

import com.example.nychighschool.data.api.NYCSchoolService
import com.example.nychighschool.data.repository.datasource.details.SchoolDetailsRemoteDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolRemoteDataSource
import com.example.nychighschool.data.repository.datasourceimpl.detailsImpl.SchoolDetailsRemoteDataSourceImpl
import com.example.nychighschool.data.repository.datasourceimpl.listImpl.HighSchoolRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {
    @Singleton
    @Provides
    fun getHighSchoolRemoteDataModule(
        highNYCSchoolService: NYCSchoolService
    ): HighSchoolRemoteDataSource {
        return HighSchoolRemoteDataSourceImpl(highNYCSchoolService)
    }

    @Singleton
    @Provides
    fun getHighSchoolDetailsRemoteDataModule(
        highNYCSchoolService: NYCSchoolService
    ): SchoolDetailsRemoteDataSource {
        return SchoolDetailsRemoteDataSourceImpl(highNYCSchoolService)
    }
}
