package com.example.nychighschool.ui.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModelProvider
import com.example.nychighschool.Injector
import com.example.nychighschool.ui.theme.NYCHighSchoolTheme
import com.example.nychighschool.ui.view.MainView
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModelFactory
import javax.inject.Inject

class MainActivity : ComponentActivity() {

    @Inject
    lateinit var factory: HighSchoolViewModelFactory
    lateinit var highSchoolViewModel: HighSchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as Injector).createHighSchoolSubComponent()
            .inject(this)
        highSchoolViewModel =
            ViewModelProvider(this, factory = factory).get(HighSchoolViewModel::class.java)
        setContent {
            NYCHighSchoolTheme {
                MainView(viewModel = highSchoolViewModel)
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NYCHighSchoolTheme {
        Greeting("Android")
    }
}
