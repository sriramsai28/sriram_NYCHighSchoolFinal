package com.example.nychighschool.ui.view

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewModelScope
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HighSchoolListViewScreen(
    viewModel: HighSchoolViewModel,
    onClick: ((schoolName: String) -> Unit)
) {
    val highSchoolList = viewModel.listLiveData.observeAsState(initial = null)
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = "GetData", block = {
        viewModel.getHighSchool()
    })
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("New York State High School", color = Color.White) },
                backgroundColor = Color.Blue
            )
        },

        scaffoldState = scaffoldState,
        backgroundColor = Color.Cyan
    ) {
        Column {
            Text(
                text = "Below is the list of New York State High Schools",
                modifier = Modifier.padding(20.dp)
            )
            LazyColumn(
                modifier = Modifier
                    .padding(16.dp)
                    .weight(1.0f),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                contentPadding = PaddingValues(vertical = 16.dp)

            ) {
                highSchoolList.value?.let {
                    items(items = it) {
                        HighSchoolCardView(highSchoolListItem = it) {
                            viewModel.viewModelScope.launch {
                                it.dbn?.let { name -> onClick.invoke(name) }
                            }
                        }
                    }
                }
            }
        }
    }
}
