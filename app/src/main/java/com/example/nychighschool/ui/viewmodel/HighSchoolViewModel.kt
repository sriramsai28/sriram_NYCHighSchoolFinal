package com.example.nychighschool.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nychighschool.data.details.DetailsListItem
import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import com.example.nychighschool.domain.usecase.GetHighSchoolUseCase
import com.example.nychighschool.domain.usecase.GetSchoolDetailsUseCase
import javax.inject.Inject

class HighSchoolViewModel @Inject constructor(
    private val getHighSchoolUseCase: GetHighSchoolUseCase,
    private val getSchoolDetailsUseCase: GetSchoolDetailsUseCase
) : ViewModel() {
    val listLiveData: MutableLiveData<List<HighSchoolListItem>> by lazy {
        MutableLiveData()
    }

    val detailsLiveData: MutableLiveData<DetailsListItem> by lazy {
        MutableLiveData()
    }

    val schoolName: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    suspend fun getHighSchool() {
        val highSchoolList = getHighSchoolUseCase.execute()
        listLiveData.value = highSchoolList
    }

    suspend fun getSchoolDetails(schoolName: String) {
        val schoolDetails = getSchoolDetailsUseCase.execute(schoolName)
        detailsLiveData.value = schoolDetails
    }

    suspend fun setSchoolName(schoolName: String) {
        schoolName.plus(schoolName)
    }
}
