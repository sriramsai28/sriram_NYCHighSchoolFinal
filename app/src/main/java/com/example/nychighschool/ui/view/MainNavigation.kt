package com.example.nychighschool.ui.view

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.coroutineContext

const val startDestination = "home"
const val detailRout = "details"
const val schoolNameArgs = "schoolName"

fun NavGraphBuilder.home(
    viewModel: HighSchoolViewModel,
    navHostController: NavHostController
) {
    composable(
        route = startDestination
    ) {
        HighSchoolListViewScreen(
            viewModel = viewModel
        ) {
            navHostController.navigateToDetails(it)
        }
    }
}

fun NavGraphBuilder.details(
    viewModel: HighSchoolViewModel,
    navHostController: NavHostController
) {
    composable(
        route = "$detailRout/schoolName/{$schoolNameArgs}"
    ) { navBackStackEntry ->
        val schoolName = checkNotNull(
            navBackStackEntry.arguments?.getString(
                schoolNameArgs
            )
        )
        HighSchoolDetailView(schoolName = schoolName, viewModel = viewModel)
    }
}
