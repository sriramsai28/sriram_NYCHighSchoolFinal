package com.example.nychighschool.ui.di.core

import com.example.nychighschool.data.repository.HighSchoolRepositoryImpl
import com.example.nychighschool.data.repository.datasource.details.SchoolDetailsRemoteDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolCacheDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolLocalDataSource
import com.example.nychighschool.data.repository.datasource.list.HighSchoolRemoteDataSource
import com.example.nychighschool.data.repository.datasourceimpl.SchoolDetailsRepositoryImpl
import com.example.nychighschool.domain.repository.HighSchoolRepository
import com.example.nychighschool.domain.repository.SchoolDetailsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun getHighSchoolRepository(
        localDataSource: HighSchoolLocalDataSource,
        remoteDataSource: HighSchoolRemoteDataSource,
        cacheDataSource: HighSchoolCacheDataSource
    ): HighSchoolRepository {
        return HighSchoolRepositoryImpl(cacheDataSource, localDataSource, remoteDataSource)
    }

    @Singleton
    @Provides
    fun getHighSchoolDetailsRepository(
        schoolDetailsRemoteDataSource: SchoolDetailsRemoteDataSource
    ): SchoolDetailsRepository {
        return SchoolDetailsRepositoryImpl(schoolDetailsRemoteDataSource)
    }
}
