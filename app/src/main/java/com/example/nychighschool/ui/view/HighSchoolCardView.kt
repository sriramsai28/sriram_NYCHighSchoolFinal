package com.example.nychighschool.ui.view

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nychighschool.data.model.listdata.HighSchoolListItem

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HighSchoolCardView(
    highSchoolListItem: HighSchoolListItem,
    onClick: (() -> Unit)
) {
    Card(elevation = 18.dp, onClick = onClick) {
        Column(Modifier.padding(16.dp)) {
            highSchoolListItem.schoolName?.let { schoolName ->
                Text(
                    text = schoolName,
                    textAlign = TextAlign.Right,
                    modifier = Modifier.align(CenterHorizontally)
                )
            }
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp)
            )
            Text(text = "Contact", fontSize = 10.sp)
            Row {
                highSchoolListItem.schoolEmail?.let { email ->
                    Text(
                        text = "Email: $email",
                        fontSize = 12.sp,
                        modifier = Modifier.weight(2f, fill = true)

                    )
                }
                highSchoolListItem.phoneNumber?.let { phoneNumber ->
                    Text(
                        text = "Phone: $phoneNumber",
                        fontSize = 12.sp
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun CardPreview() {
    HighSchoolCardView(HighSchoolListItem("123", "test@gmail.com", "Test School Name", "123456789")){

    }
}
