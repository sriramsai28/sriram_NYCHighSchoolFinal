package com.example.nychighschool.ui.di

import com.example.nychighschool.domain.usecase.GetHighSchoolUseCase
import com.example.nychighschool.domain.usecase.GetSchoolDetailsUseCase
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class HighSchoolViewModelModule {

    @HighSchoolScope
    @Provides
    fun getHighSchoolViewModel(
        getHighSchoolUseCase: GetHighSchoolUseCase,
        getSchoolDetailsUseCase: GetSchoolDetailsUseCase
    ): HighSchoolViewModelFactory {
        return HighSchoolViewModelFactory(getHighSchoolUseCase, getSchoolDetailsUseCase)
    }
}
