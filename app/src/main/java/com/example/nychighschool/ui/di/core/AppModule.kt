package com.example.nychighschool.ui.di.core

import android.content.Context
import com.example.nychighschool.ui.di.HighSchoolSupComponent
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [HighSchoolSupComponent::class])
class AppModule(private val context: Context) {
    @Singleton
    @Provides
    fun provideApplicationContext(): Context {
        return context.applicationContext
    }
}
