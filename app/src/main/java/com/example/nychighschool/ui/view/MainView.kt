package com.example.nychighschool.ui.view

import androidx.compose.runtime.Composable
import androidx.navigation.compose.rememberNavController
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel

@Composable
internal fun MainView(
    viewModel: HighSchoolViewModel
) {
    val navController = rememberNavController()
    MainNavHost(viewModel = viewModel, navController = navController)
}


