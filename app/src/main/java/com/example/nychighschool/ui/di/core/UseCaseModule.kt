package com.example.nychighschool.ui.di.core

import com.example.nychighschool.domain.repository.HighSchoolRepository
import com.example.nychighschool.domain.repository.SchoolDetailsRepository
import com.example.nychighschool.domain.usecase.GetHighSchoolUseCase
import com.example.nychighschool.domain.usecase.GetSchoolDetailsUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {
    @Singleton
    @Provides
    fun getHighSchoolUseCase(
        highSchoolRepository: HighSchoolRepository
    ): GetHighSchoolUseCase {
        return GetHighSchoolUseCase(highSchoolRepository)
    }

    @Singleton
    @Provides
    fun getSchoolDetailsUseCase(
        schoolDetailsRepository: SchoolDetailsRepository
    ): GetSchoolDetailsUseCase {
        return GetSchoolDetailsUseCase(schoolDetailsRepository)
    }
}
