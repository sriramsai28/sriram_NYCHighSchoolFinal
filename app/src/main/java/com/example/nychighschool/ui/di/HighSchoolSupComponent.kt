package com.example.nychighschool.ui.di

import com.example.nychighschool.ui.activity.MainActivity
import dagger.Subcomponent

@HighSchoolScope
@Subcomponent(modules = [HighSchoolViewModelModule::class])
interface HighSchoolSupComponent {
    fun inject(activity: MainActivity)

    @Subcomponent.Factory
    interface Factory {
        fun create(): HighSchoolSupComponent
    }
}
