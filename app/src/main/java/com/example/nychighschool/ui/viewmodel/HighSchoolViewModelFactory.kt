package com.example.nychighschool.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nychighschool.domain.usecase.GetHighSchoolUseCase
import com.example.nychighschool.domain.usecase.GetSchoolDetailsUseCase
import javax.inject.Inject

class HighSchoolViewModelFactory @Inject constructor(
    private val getHighSchoolUseCase: GetHighSchoolUseCase,
    private val getSchoolDetailsUseCase: GetSchoolDetailsUseCase,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HighSchoolViewModel(getHighSchoolUseCase, getSchoolDetailsUseCase) as T
    }
}
