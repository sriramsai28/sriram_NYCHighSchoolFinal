package com.example.nychighschool.ui.di.core

import com.example.nychighschool.data.repository.datasource.list.HighSchoolCacheDataSource
import com.example.nychighschool.data.repository.datasourceimpl.listImpl.HighSchoolCacheDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CacheDataSourceModule {
    @Singleton
    @Provides
    fun getHighSchoolCacheDataSource(): HighSchoolCacheDataSource {
        return HighSchoolCacheDataSourceImpl()
    }
}
