package com.example.nychighschool.ui.view

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.example.nychighschool.ui.viewmodel.HighSchoolViewModel

@Composable
fun MainNavHost(
    viewModel: HighSchoolViewModel,
    navController: NavHostController
) {
    NavHost(navController = navController, startDestination = startDestination) {
        home(
            viewModel = viewModel,
            navHostController = navController
        )
        details(
            viewModel = viewModel,
            navHostController = navController
        )
    }
}

fun NavController.navigateToDetails(schoolName: String) {
    val navRout = "$detailRout/schoolName/$schoolName"
    navigate(navRout)
}
