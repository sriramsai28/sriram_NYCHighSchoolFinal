package com.example.nychighschool.domain.usecase

import com.example.nychighschool.data.details.DetailsListItem
import com.example.nychighschool.domain.repository.SchoolDetailsRepository
import javax.inject.Inject

class GetSchoolDetailsUseCase @Inject constructor(
    private val detailsRepository: SchoolDetailsRepository
) {
    suspend fun execute(schoolName: String): DetailsListItem? =
        detailsRepository.getHighSchoolList(schoolName)
}
