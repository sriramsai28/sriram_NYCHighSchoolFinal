package com.example.nychighschool.domain.repository

import com.example.nychighschool.data.details.DetailsListItem

interface SchoolDetailsRepository {
    suspend fun getHighSchoolList(schoolName: String): DetailsListItem?
}
