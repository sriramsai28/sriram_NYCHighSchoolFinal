package com.example.nychighschool.domain.usecase

import com.example.nychighschool.data.model.listdata.HighSchoolListItem
import com.example.nychighschool.domain.repository.HighSchoolRepository
import javax.inject.Inject

// Probably use invoke to call with class name. e,g getHighSchoolUseCase(repo)
class GetHighSchoolUseCase @Inject constructor(
    private val highSchoolRepository: HighSchoolRepository
) {
    suspend fun execute(): List<HighSchoolListItem> =
        highSchoolRepository.getHighSchoolList()

    /*suspend operator fun invoke() {

    }*/
}
