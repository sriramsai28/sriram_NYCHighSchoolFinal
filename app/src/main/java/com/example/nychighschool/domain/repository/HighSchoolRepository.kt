package com.example.nychighschool.domain.repository

import com.example.nychighschool.data.model.listdata.HighSchoolListItem

interface HighSchoolRepository {
    suspend fun getHighSchoolList(): List<HighSchoolListItem>
}
