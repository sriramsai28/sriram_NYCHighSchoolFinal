package com.example.nychighschool

import com.example.nychighschool.ui.di.HighSchoolSupComponent

interface Injector {
    fun createHighSchoolSubComponent(): HighSchoolSupComponent
}
