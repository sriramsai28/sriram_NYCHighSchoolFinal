package com.example.nychighschool

import android.app.Application
import com.example.nychighschool.ui.di.HighSchoolSupComponent
import com.example.nychighschool.ui.di.core.*

class App : Application(), Injector {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .retrofitModule(RetrofitModule(BuildConfig.BASE_URL))
            .remoteDataSourceModule(RemoteDataSourceModule()).build()
    }

    override fun createHighSchoolSubComponent(): HighSchoolSupComponent {
        return appComponent.getHighSchoolSubComponent().create()
    }
}
